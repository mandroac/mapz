﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheGame
{
    enum EnemyType
    { 
        DefaultGhost, BoostedGhost
    }

    
    
    abstract class Enemy
    {
        public int HP { get; set; }

        public abstract Enemy copyEnemy();

        public abstract PictureBox getImage();

        public void Damage(int _dmg)
        {
            this.HP -= _dmg;
            if (this.HP <= 0) this.getImage().Image = null;
        }

    }
}
