﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheGame
{
    class MovementFacade
    {
        private PictureBox background;
        private PictureBox character;

        public MovementFacade(PictureBox _background, PictureBox _character) 
        {
            this.background = _background;
            this.character = _character;
        }

        public void BackgroundMove()
        {
            if (background.Left < -1400) background.Left = 0;
            background.Left -= 15;
        }

        public void CharactrerUp() 
        {
            if (character.Top > 350)
            {
                character.Top -= 5;
                character.Image = Properties.Resources.elf_run;
            }
        }

        public void CharactrerDown()
        {
            if (character.Top < 499)
            {
                character.Top += 5;
                character.Image = Properties.Resources.elf_run;
            }
        }

    }
}
