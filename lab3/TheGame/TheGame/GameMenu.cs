﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheGame
{
    public partial class GameMenu : Form
    {
        public int newscore { get; set; }
        public GameMenu()
        {
            InitializeComponent();
            newscore = 0;
        }

        private void label4_Click(object sender, EventArgs e)
        {
            HighScores highScores = HighScores.Intitialize();
            List<string> scoreList = highScores.getScoreList();
            
            scoreSelector1.Visible = true;
            if (this.DialogResult == DialogResult.OK) newscore = scoreSelector1.currentscore;
            //Console.WriteLine(newscore);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_MouseLeave(object sender, EventArgs e)
        {
            Label label = sender as Label;
            label.BackColor = Color.Orange;
        }

        private void label1_MouseEnter(object sender, EventArgs e)
        {
            Label label = sender as Label;
            label.BackColor = Color.DarkOrange;
        }

        private void label3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
