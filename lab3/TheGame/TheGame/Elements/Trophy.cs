﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame
{
    class Trophy : Item
    {
        public int GenerateValue()
        {
            return 10;
        }
        public ItemTypes getItemType()
        {
            return ItemTypes.TROPHY;
        }

        public string Message()
        {
            return "Glorious!";
        }
    }
}
