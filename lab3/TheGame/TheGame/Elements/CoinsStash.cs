﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame
{
    class CoinStash : Item
    {
        public int GenerateValue()
        {
            Random rnd = new Random();
            return (int)rnd.Next(5,10);
        }
        public ItemTypes getItemType()
        {
            return ItemTypes.COIN_STASH;
        }

        public string Message()
        {
            return "Money doesn't grow on the tree. It falls from the sky!";
        }
    }
}
