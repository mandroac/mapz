﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.Factories
{
    public interface Factory
    {
        Item GenerateItem();
    }
}
