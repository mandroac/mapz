﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheGame
{
    class Elf
    {
        private int HP;
        private Point TopLeftPostition;
        public Attack Weapon { get; set; }

        public Elf(Point _position) {
            HP = 3;
            TopLeftPostition = _position;
        }

        public void Move(int right, int top) {
            TopLeftPostition.X += right;
            TopLeftPostition.Y += top;
        }
    }
}
