﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.Memento
{
    public class GameMemento
    {
        public int ID {get;}
        public int SCORE { get; }
        public int COINS { get; }
        public DateTime Date { get; }

        public GameMemento(int score, int coins)
        {
            Random rnd = new Random();
            this.ID = rnd.Next(0,10000);
            this.Date = DateTime.Now;
            this.SCORE = score;
            this.COINS = coins;
        }

    }
}
