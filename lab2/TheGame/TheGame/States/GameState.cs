﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame
{
    public abstract class GameState
    {
        protected Game game;

        public void SetGame(Game _game)
        {
            this.game = _game;
        }

        public abstract void Escape();
    }
}
