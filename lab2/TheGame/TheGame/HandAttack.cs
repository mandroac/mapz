﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheGame
{
    class HandAttack : Attack
    {
        private PictureBox elf;
        public Enemy enemy { get; set; }
        public HandAttack(PictureBox Character)
        {
            this.elf = Character;
        }

        private Rectangle AttackArea()
        {
            Point temp =  this.elf.Location;
            temp.X += elf.Image.Width;
            temp.Y += elf.Image.Height / 4;
            Rectangle result = new Rectangle(temp, new Size(30,30));
            return result;
        }

        public void Shoot(Point start, Point end)
        {
            if (this.enemy.getImage().Bounds.IntersectsWith(this.AttackArea())) this.enemy.Damage(1);
        }
    }
}
