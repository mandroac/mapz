﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.Factories
{
    class CoinFactory : Factory
    {
        public  Item GenerateItem()
        {
            return new Coin();
        }
    }
}
