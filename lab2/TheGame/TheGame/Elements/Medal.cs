﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame
{
    class Medal : Item
    {
        public int GenerateValue()
        {
            Random rnd = new Random();
            return (int)rnd.Next(50,100);
        }

        public ItemTypes getItemType()
        {
            return ItemTypes.MEDAL;
        }

        public string Message()
        {
            return "The medal of honor!";
        }


    }
}
