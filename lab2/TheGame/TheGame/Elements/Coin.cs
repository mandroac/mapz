﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame
{
    class Coin : Item
    {
        public int GenerateValue()
        {
            return 1;
        }
        public ItemTypes getItemType()
        {
            return ItemTypes.COIN;
        }

        public string Message()
        {
            return "Money, money, money!";
        }
    }
}
