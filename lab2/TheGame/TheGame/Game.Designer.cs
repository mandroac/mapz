﻿namespace TheGame
{
    partial class Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.labelCoins = new System.Windows.Forms.Label();
            this.labelScore = new System.Windows.Forms.Label();
            this.pictureItem2 = new System.Windows.Forms.PictureBox();
            this.pictureItem1 = new System.Windows.Forms.PictureBox();
            this.Elf = new System.Windows.Forms.PictureBox();
            this.screen = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Elf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.screen)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // labelCoins
            // 
            this.labelCoins.BackColor = System.Drawing.Color.Indigo;
            this.labelCoins.Font = new System.Drawing.Font("MV Boli", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCoins.ForeColor = System.Drawing.Color.Yellow;
            this.labelCoins.Location = new System.Drawing.Point(679, 9);
            this.labelCoins.Name = "labelCoins";
            this.labelCoins.Size = new System.Drawing.Size(193, 41);
            this.labelCoins.TabIndex = 5;
            this.labelCoins.Text = "Coins: 0";
            // 
            // labelScore
            // 
            this.labelScore.BackColor = System.Drawing.Color.Indigo;
            this.labelScore.Font = new System.Drawing.Font("MV Boli", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelScore.ForeColor = System.Drawing.Color.Yellow;
            this.labelScore.Location = new System.Drawing.Point(26, 9);
            this.labelScore.Name = "labelScore";
            this.labelScore.Size = new System.Drawing.Size(219, 41);
            this.labelScore.TabIndex = 6;
            this.labelScore.Text = "Score: 0";
            // 
            // pictureItem2
            // 
            this.pictureItem2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(16)))), ((int)(((byte)(44)))));
            this.pictureItem2.Image = global::TheGame.Properties.Resources.coin;
            this.pictureItem2.Location = new System.Drawing.Point(1500, 426);
            this.pictureItem2.Name = "pictureItem2";
            this.pictureItem2.Size = new System.Drawing.Size(57, 35);
            this.pictureItem2.TabIndex = 4;
            this.pictureItem2.TabStop = false;
            // 
            // pictureItem1
            // 
            this.pictureItem1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(16)))), ((int)(((byte)(44)))));
            this.pictureItem1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureItem1.Image = global::TheGame.Properties.Resources.coin;
            this.pictureItem1.Location = new System.Drawing.Point(536, 426);
            this.pictureItem1.Name = "pictureItem1";
            this.pictureItem1.Size = new System.Drawing.Size(57, 35);
            this.pictureItem1.TabIndex = 2;
            this.pictureItem1.TabStop = false;
            // 
            // Elf
            // 
            this.Elf.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(16)))), ((int)(((byte)(44)))));
            this.Elf.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Elf.Image = global::TheGame.Properties.Resources.elf_idle;
            this.Elf.Location = new System.Drawing.Point(143, 396);
            this.Elf.Name = "Elf";
            this.Elf.Size = new System.Drawing.Size(76, 86);
            this.Elf.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Elf.TabIndex = 1;
            this.Elf.TabStop = false;
            // 
            // screen
            // 
            this.screen.Image = global::TheGame.Properties.Resources.BACKGROUND;
            this.screen.Location = new System.Drawing.Point(0, 0);
            this.screen.Name = "screen";
            this.screen.Size = new System.Drawing.Size(2800, 592);
            this.screen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.screen.TabIndex = 0;
            this.screen.TabStop = false;
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 585);
            this.Controls.Add(this.labelScore);
            this.Controls.Add(this.labelCoins);
            this.Controls.Add(this.pictureItem2);
            this.Controls.Add(this.pictureItem1);
            this.Controls.Add(this.Elf);
            this.Controls.Add(this.screen);
            this.Cursor = System.Windows.Forms.Cursors.Cross;
            this.Name = "Game";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.pictureItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Elf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.screen)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox screen;
        private System.Windows.Forms.PictureBox Elf;
        private System.Windows.Forms.Timer timer1;
        protected System.Windows.Forms.PictureBox pictureItem1;
        protected System.Windows.Forms.PictureBox pictureItem2;
        private System.Windows.Forms.Label labelCoins;
        private System.Windows.Forms.Label labelScore;
    }
}

