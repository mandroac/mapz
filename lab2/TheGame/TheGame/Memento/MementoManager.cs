﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.Memento
{
    class MementoManager
    {
        private List<GameMemento> mementos = new List<GameMemento>();
        private Game OriginGame;

        public MementoManager(Game _game)
        {
            this.OriginGame = _game;
        }

        public void Backup()
        {
            this.mementos.Add(this.OriginGame.Save());
        }

        public void RemoveBackup(int RemoveID)
        {
            foreach (var RemoveItem in mementos)
            {
                if (RemoveItem.ID == RemoveID) this.mementos.Remove(RemoveItem);
            }
        }
    }
}