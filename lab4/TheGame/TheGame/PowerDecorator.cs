﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame
{
    class PowerDecorator : BowDecorator
    {
        public PowerDecorator(Bow _bow) : base(_bow)
        {
            this.setDamage(2);
        }
    }
}
