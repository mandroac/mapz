﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame
{
    abstract class Bow : Attack
    {
        private int damage;
        private bool isPierced;

        public abstract void Shoot(Point start, Point end);

        public int Hit()
        {
            return damage;
        }

        protected void setDamage(int _damage)
        {
            this.damage = _damage;
        }
        protected void setPierced(bool _pierce)
        {
            this.isPierced = _pierce;
        }

    }
}