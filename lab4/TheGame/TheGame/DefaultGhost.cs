﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheGame
{
    class DefaultGhost : Enemy
    {
        private EnemyType GhostType { get; }
        private PictureBox image;
        public override Enemy copyEnemy()
        {
            Console.WriteLine("New enemy is being created at the moment");
            return (Enemy) this.MemberwiseClone();

        }

        public override PictureBox getImage()
        {
            return this.image;
        }

        public DefaultGhost() 
        {
            this.HP = 1;
            GhostType = EnemyType.DefaultGhost;
        }
    }
}
