﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheGame
{
    class Arrow : Bow
    {
        private PictureBox arrow;
        private double angle;

        public Arrow(PictureBox _arrow)
        {
            this.setDamage(1);
            this.arrow = _arrow;
            this.setPierced(false);
        }
        public override void Shoot(Point start, Point end)
        {
            angle = Math.Atan2((start.X - end.X), (start.Y - end.Y));

            arrow.Left = end.X;
            arrow.Top = end.Y;
        }
    }
}
