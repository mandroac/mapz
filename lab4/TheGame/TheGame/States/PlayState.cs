﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.States
{
    class PlayState : GameState
    {
        public override void Escape()
        {
            this.game.MyGameMenu.ShowDialog();
            this.game.TransitionTo(new EscapeState());
        }
    }
}
