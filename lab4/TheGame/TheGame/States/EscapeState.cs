﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame.States
{
    class EscapeState : GameState
    {
        public override void Escape()
        {
            this.game.MyGameMenu.Hide();
            this.game.TransitionTo(new PlayState());
        }
    }
}
