﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheGame
{
    public partial class ScoreSelector : UserControl
    {
        public int currentscore { get; set; }

        private ScoreSelectorMediator scoreSelectorMediator;
        public ScoreSelector()
        {
            InitializeComponent();

            scoreSelectorMediator = new ScoreSelectorMediator(this);
            HighScores MyHighScores = HighScores.Intitialize();
            List<string> scoreList = MyHighScores.getScoreList();
            foreach (var str in scoreList)
            {
                listBox1.Items.Add(str);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            scoreSelectorMediator.OK_Button_Clicked(Convert.ToInt32(listBox1.SelectedItem));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            scoreSelectorMediator.CANCEL_Button_Clicked();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                button2.Enabled = true;
            }
            else button2.Enabled = false;
        }
    }
}
