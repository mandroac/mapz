﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheGame
{
    abstract class BowDecorator : Bow
    {
        protected Bow bow;

        public BowDecorator(Bow _bow)
        {
            this.bow = _bow;
        }
        public override void Shoot(Point start, Point end)
        {
            if(bow != null) bow.Shoot(start, end);
        }
    }
}
