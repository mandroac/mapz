﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TheGame
{
    class HighScores
    {
        private List<string> scores;
        private static HighScores instance;
        private HighScores()
        {
            scores = File.ReadAllLines("highscores.txt").ToList();
        }

        public static HighScores Intitialize() 
        {
            if (instance == null) instance = new HighScores();
            return instance;
        }

        public List<string> getScoreList()
        {
            return scores;
        }
    }
}