﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TheGame.Factories;
using TheGame.Memento;
using TheGame.States;

namespace TheGame
{
    public partial class Game : Form
    {
        private bool up, down, right; 
        private Random rand;
        private int coins = 0, score = 0, scorecnt;
        private Item CurrentItem1, CurrentItem2;
        public GameMenu MyGameMenu { get; }
        private List<Enemy> enemies;
        private MovementFacade movFacade;
        private GameState MyGameState;


        public Game()
        {
            InitializeComponent();
            Bitmap curs = new Bitmap(Properties.Resources.target);
            Cursor = new Cursor(curs.GetHicon());
            rand = new Random();
            CurrentItem1 = CurrentItem2 = new Coin();
            TransitionTo(new PlayState());
            MyGameMenu = new GameMenu();
            MyGameMenu.ShowDialog();
            movFacade = new MovementFacade(screen, Elf);
            enemies = new List<Enemy>();

        }

        private void GenerateEnemy()
        {
            int temp = rand.Next(0, 100);
            Enemy enemy;
            if (temp <= 80) enemy = new DefaultGhost();
            else enemy = new BoostedGhost();
            enemies.Add(enemy);
        }

        private void RemoveEnemy(Enemy _enemyToRemove)
        {
            this.enemies.Remove(_enemyToRemove);
        }

        private void EnemiesUpgrade()
        {
            foreach (var _enemy in enemies)
            {
                _enemy.HP++;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //ScoreListener();
            Character_movement();
            Background();
            Items();
        }

        public GameMemento Save()
        {
            return new GameMemento(this.score,this.coins);
        }

        public void Restore(GameMemento _memento)
        {
            this.score = _memento.SCORE;
            this.coins = _memento.COINS;
        }

        public void TransitionTo(GameState _gameState)
        {
            Console.WriteLine("StateChange");
            this.MyGameState = _gameState;
            this.MyGameState.SetGame(this);
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        { 
                switch (e.KeyCode)
                {
                    case Keys.Right:
                    if (!right && !up && !down )
                    {
                        Elf.Image = Properties.Resources.elf_run;
                    }
                    right = true;
                    break;
                    case Keys.Up:
                    if (!right && !(up || down))
                    {
                        Elf.Image = Properties.Resources.elf_run;
                    }
                    up = true;
                    break;
                    case Keys.Down:
                    if (!right && !(up || down))
                    {
                        Elf.Image = Properties.Resources.elf_run;
                    }
                    down = true;
                    break;
                case Keys.Escape:
                    MyGameState.Escape();
                    break;

                }
           
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Right:
                    right = false;
                    if (!right && !(up || down))
                    {
                        Elf.Image = Properties.Resources.elf_idle;
                    }
                    break;
                case Keys.Up:
                    up = false;
                    if (!right && !(up || down))
                    {
                        Elf.Image = Properties.Resources.elf_idle;
                    }
                    break;
                case Keys.Down:
                    down = false;
                    if (!right && !(up || down))
                    {
                        Elf.Image = Properties.Resources.elf_idle;
                    }
                    break;
            }
            
        }

        void Character_movement()
        {
            if (up)
            {
                movFacade.CharactrerUp();
            }
            if (down)
            {
                movFacade.CharactrerDown();
            }
        }

        void Background() 
        {
            if (right)
            {
                movFacade.BackgroundMove();
                scorecnt++;
                if ((scorecnt%5 == 0)) { score++; labelScore.Text = "Score: " + this.score; }
            }

        }


        public Item ItemCreator(Factory factory)
        {
            Item newItem = factory.GenerateItem();
            return newItem;
        }

        public Item RandomizeFactory()
        {
            int temp = rand.Next(0, 100);

            Item newItem;
            if (temp >= 0 && temp <= 30) newItem = ItemCreator(new CoinFactory());
            else if (temp > 30 && temp <= 60) newItem =  ItemCreator(new TrophyFactory());
            else if (temp > 60 && temp <= 80) newItem = ItemCreator(new CoinStashFactory());
            else newItem = ItemCreator(new MedalFactory());

            return newItem;
        }

        void ChangePicture(PictureBox pBox, Item itm)
        {
            switch (itm.getItemType())
            {
                case ItemTypes.COIN:
                    pBox.Image = Properties.Resources.coin;
                    break;
                case ItemTypes.COIN_STASH:
                    pBox.Image = Properties.Resources.coins_stash;
                    break;
                case ItemTypes.MEDAL:
                    pBox.Image = Properties.Resources.medal;
                    break;
                case ItemTypes.TROPHY:
                    pBox.Image = Properties.Resources.trophy;
                    break;
            }
        }
        


        public void Collect(Item itm)
        {
            switch (itm.getItemType())
            {
                case ItemTypes.COIN:
                case ItemTypes.COIN_STASH:
                    this.coins += itm.GenerateValue();
                    labelCoins.Text = "Coins: " + this.coins;
                    break;
                case ItemTypes.MEDAL:
                case ItemTypes.TROPHY:
                    this.score += itm.GenerateValue();
                    labelScore.Text = "Score: " + this.score;
                    break;
            }
        }

        public void Items()
        {
            if (right)
            {
                if (pictureItem1.Left < -20) {
                    CurrentItem1 = RandomizeFactory();
                    ChangePicture(pictureItem1, CurrentItem1);
                    pictureItem1.Location = new Point (rand.Next(950, 2000), rand.Next(350,499));
                }
                if (pictureItem2.Left < -20)
                {
                    CurrentItem2 = RandomizeFactory();
                    ChangePicture(pictureItem2, CurrentItem2);
                    pictureItem2.Location = new Point(rand.Next(950, 2000), rand.Next(350, 499));
                }
                if (Elf.Bounds.IntersectsWith(pictureItem1.Bounds)) 
                {
                    pictureItem1.Visible = false;
                    Collect(CurrentItem1);
                    CurrentItem1 = RandomizeFactory();
                    ChangePicture(pictureItem1, CurrentItem1);
                    pictureItem1.Location = new Point(rand.Next(950, 2000), rand.Next(350, 499));
                    pictureItem1.Visible = true;
                }
                if (Elf.Bounds.IntersectsWith(pictureItem2.Bounds))
                {
                    pictureItem2.Visible = false;
                    Collect(CurrentItem2);
                    CurrentItem2 = RandomizeFactory();
                    ChangePicture(pictureItem2, CurrentItem2);
                    pictureItem2.Location = new Point(rand.Next(950, 2000), rand.Next(350, 499));
                    pictureItem2.Visible = true;
                }

                pictureItem1.Left -= 15;
                pictureItem2.Left -= 15;
            }
        }

        void ScoreListener()
        {
            if (MyGameMenu.DialogResult == DialogResult.OK) this.score = 100;
        }
    }   
}
