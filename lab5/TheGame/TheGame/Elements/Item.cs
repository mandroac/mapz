﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame
{
    public enum ItemTypes { 
        COIN,
        COIN_STASH,
        TROPHY,
        MEDAL
    }

    public interface Item
    {
        string Message();

        ItemTypes getItemType();

        int GenerateValue();
    }
}
