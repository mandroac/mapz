﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheGame
{
    public interface Attack
    {
        void Shoot(Point start, Point end);
    }
}
