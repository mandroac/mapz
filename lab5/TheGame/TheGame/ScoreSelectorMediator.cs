﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGame
{
    class ScoreSelectorMediator
    {
        private ScoreSelector scoreSelector;

        public ScoreSelectorMediator(ScoreSelector _scoreSelector)
        {
            scoreSelector = _scoreSelector;
        }

        public void OK_Button_Clicked(int score)
        { 
            scoreSelector.currentscore = score;
        }

        public void CANCEL_Button_Clicked()
        {
            scoreSelector.Visible = false;
        }
    }
}
