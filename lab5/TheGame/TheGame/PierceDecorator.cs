﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheGame
{
    class PierceDecorator : BowDecorator
    {
        public PierceDecorator(Bow _bow) : base(_bow)
        {
            this.setPierced(true);
        }

    }
}
