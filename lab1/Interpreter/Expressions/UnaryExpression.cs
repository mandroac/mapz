﻿using Interpreter.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter.Expressions
{
    class UnaryExpression : Expression
    {
        private Expression expr1;
        private char operation;

        public UnaryExpression(char _operation, Expression _expr1)
        {
            operation = _operation;
            expr1 = _expr1;

        }
        public Value Eval()
        {
            switch (operation)
            {
                case '-': return new NumberValue(-expr1.Eval().asDouble());
                case '+': 
                default: return expr1.Eval();
            }
        }
        public string toString()
        {
            return String.Format("{0} {1}", operation, expr1);
        }
    }
}
