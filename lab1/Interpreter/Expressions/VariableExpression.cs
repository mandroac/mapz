﻿using Interpreter.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter.Expressions
{
    class VariableExpression : Expression
    {
        private string name;

        public VariableExpression(string name)
        {
            this.name = name;
        }

        public Value Eval()
        {
           if (!Variables.exists(name)) throw new Exception("CONSTANT DOES NOT EXIST");
           return Variables.get(name);
        }
         
        public override string ToString()
        {
            return String.Format("{0} [{1}]", name, Variables.get(name));
        }
    }
}
