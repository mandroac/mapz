﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interpreter.Library;

namespace Interpreter
{
    class ValueExpression: Expression
    {
        private Value value;

        public ValueExpression(double value) {
            this.value = new NumberValue(value);
        }

        public ValueExpression(string value)
        {
            this.value = new StringValue(value);
        }
        public Value Eval()
        {
            return value; 
        }

        public override string ToString() {
            return value.asString();
        }
    }
}
