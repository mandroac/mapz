﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interpreter.Library;

namespace Interpreter.Expressions
{
    class ConditionalExpression : Expression
    {
        public enum Operator {
            /*PLUS, MINUS, MULTIPLY, DIVIDE,*/ EQUALS, NOT_EQUALS, LT, LTEQ, GT, GTEQ, //AND, OR 
        }

        private Expression expr1, expr2;
        private Operator operation;

        public ConditionalExpression(Operator _operation, Expression _expr1, Expression _expr2)
        {
            operation = _operation;
            expr1 = _expr1;
            expr2 = _expr2;
        }

        public Value Eval()
        {
            Value value1 = expr1.Eval();
            Value value2 = expr2.Eval();

            double number1, number2;
            if (value1.GetType() == typeof(StringValue))
            {
                number1 = value1.asString().CompareTo(value2.asString());
                number2 = 0;
            }
            else {

                number1 = value1.asDouble();
                number2 = value2.asDouble();
            }


            bool result;
            switch (operation)
            {
                case Operator.LT: result = number1 < number2; break;
                case Operator.GT: result = number1 > number2; break;
                case Operator.GTEQ: result = number1 >= number2; break;
                case Operator.LTEQ: result = number1 <= number2; break;
                case Operator.NOT_EQUALS: result = number1 != number2; break;

                case Operator.EQUALS: 
                default: result = number1 == number2; break;
            }
            return new NumberValue(result);
        }

        public override string ToString()
        {
            return String.Format("{0} {1} {2}", expr1, operation, expr2);
        }
    }
}
