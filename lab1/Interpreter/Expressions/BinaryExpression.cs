﻿using Interpreter.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter
{
    class BinaryExpression : Expression
    {
        private Expression expr1, expr2;
        private char operation;

        public BinaryExpression(char _operation, Expression _expr1, Expression _expr2) {
            operation = _operation;
            expr1 = _expr1;
            expr2 = _expr2;
        }

        public Value Eval()
        {
            Value value1 = expr1.Eval();
            Value value2 = expr2.Eval();

            if (value1.GetType() == typeof(StringValue))
            {
                string string1 = value1.asString();
                switch (operation)
                {
                    case '+':
                    default: return new StringValue(string1 + value2.asString());
                }
            }
               

            double number1 = value1.asDouble();
            double number2 = value2.asDouble();


            switch (operation)
            {
                case '+': return new NumberValue(number1 + number2);
                case '-': return new NumberValue(number1 - number2);
                case '*': return new NumberValue(number1 * number2);
                case '/': return new NumberValue(number1 / number2);
                default: return new NumberValue(number1 + number2);
            }
        }

        public string toString() {
            return String.Format("{0} {1} {2}", expr1, operation, expr2);
        }

    }
}
