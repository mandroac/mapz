﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Interpreter.Library;
using Interpreter.Statements;

namespace Interpreter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string text = textBox1.Text;
            List<Token> tokens = new Lexer(text).tokenize();
            foreach (Token token in tokens) Console.WriteLine(token.type + " " + token.text);
            try
            {

                var parser = new Parser(tokens);
                parser.FunctionIsCalled += Parser_FunctionIsCalled;
                List<Statement> statements = parser.parse();
                foreach (Statement statement in statements) statement.execute();      
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }

        }

        private void Parser_FunctionIsCalled(object sender, FunctionArgs e)
        {
            string funcName = ReadFunctionName(e.FunctionalExpression.ToString());
            string funcResult = e.FunctionalExpression.Eval().ToString();
            if ( funcName == "openUrl" || funcName == "GoogleSearch" )
            {
                textBoxUrl.Text = funcResult;
                webBrowser.Navigate(textBoxUrl.Text);
            }
        }

        private void buttonGo_Click(object sender, EventArgs e)
        {
            webBrowser.Navigate(textBoxUrl.Text);
            
        }

        private string ReadFunctionName(string function)
        {
            int pos = 0;
            StringBuilder buffer = new StringBuilder();
            char current = function[pos]; 
            while (current != '(') {
                buffer.Append(current);
                pos++;
                current = function[pos];             
            }
            return buffer.ToString();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            webBrowser.GoBack();
        }

        private void buttonForward_Click(object sender, EventArgs e)
        {
            webBrowser.GoForward();
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            webBrowser.Refresh();
        }
    }
}
