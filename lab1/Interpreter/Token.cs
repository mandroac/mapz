﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter
{
    class Token
    {
        public TokenType type { get; set; }
        public string text { get; set; }

        public Token(TokenType _type, string _text) {
            type = _type;
            text = _text;
        }

        public Token() { }

    }
}
