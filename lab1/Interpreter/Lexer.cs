﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter
{
    class Lexer
    {
        private string input;
        private List<Token> tokens;
        private int pos;
        private int InputLenght;

       
        static string  OPERATOR_CHARS = "+-*/(){}=<>;,";

        private static Dictionary<string, TokenType> OPERATORS = new Dictionary<string, TokenType>
        {
            {"+", TokenType.PLUS },
            {"-", TokenType.MINUS },
            {"*", TokenType.MULT },
            {"/", TokenType.DIV },
            {"(", TokenType.LPAREN },
            {")", TokenType.RPAREN },
            {"{", TokenType.LBRACE },
            {"}", TokenType.RBRACE },
            {"=", TokenType.EQ },
            {"<", TokenType.LT },
            {">", TokenType.GT },
            {";", TokenType.SEMICOL },
            {",", TokenType.COMMA },


            {"<=", TokenType.LTEQ },
            {">=", TokenType.GTEQ },
            {"==", TokenType.EQEQ },
            {"!=", TokenType.EXCLEQ },
            

        };

        public Lexer(string _input)
        {
            input = _input;
            InputLenght = _input.Length;

            tokens = new List<Token>();
        }

        public List<Token> tokenize() {
            while (pos < InputLenght) {
                char current = peek(0);
                if (Char.IsDigit(current)) tokenizeNumber();
                else if (Char.IsLetter(current)) tokenizeWord();
                else if (current == '"') tokenizeText();
                else if (OPERATOR_CHARS.IndexOf(current) != -1) tokenizeOperator();
                else next();
            }
            return tokens;
        }

        private void tokenizeText()
        {
            char current = next(); // skip opening
            StringBuilder buffer = new StringBuilder();
            while (true)
            {
                if (current == '\\')
                {
                    current = next();
                    switch (current) {
                        case '"': current = next(); buffer.Append('"'); continue;
                        case 'n': current = next(); buffer.Append('\n'); continue;
                        case 't': current = next(); buffer.Append('\t'); continue;
                        //default: current = next(); buffer.Append('\\'); continue; // not the same as in the video
                    }
                }
                if (current == '"') break;
                buffer.Append(current);
                current = next();
            }
            next(); // skip closing

            AddToken(TokenType.TEXT, buffer.ToString());
        }

        private void tokenizeWord()
        {
            char current = peek(0);
            StringBuilder buffer = new StringBuilder();
            while (true)
            {
                if (!Char.IsLetterOrDigit(current) && (current != '_') && (current != '$')) break;
                buffer.Append(current);
                current = next();
            }
            switch (buffer.ToString())
            {
                case "print":
                case "PRINT": AddToken(TokenType.PRINT); break;
                case "if": AddToken(TokenType.IF); break;
                case "else": AddToken(TokenType.ELSE); break;
                case "while": AddToken(TokenType.WHILE); break;
                case "for": AddToken(TokenType.FOR); break;
                default: AddToken(TokenType.WORD, buffer.ToString()); break;

            }
        }

        private void tokenizeOperator()
        {
            char current = peek(0);
            if (current == '/')
            {
                if (peek(1) == '/')
                {
                    next();
                    next();
                    tokenizeOneLineComment();
                    return;
                }
                else if (peek(1) == '*')
                {
                    next();
                    next();
                    tokenizeMultilineComment();
                    return;
                }
            }

            StringBuilder buffer = new StringBuilder();
            while (true) {
                string text = buffer.ToString();
                if (!OPERATORS.ContainsKey(text + current) && text.Length != 0) {
                    TokenType newToken;
                    OPERATORS.TryGetValue(text, out newToken);
                    AddToken(newToken);
                    return;
                }
                buffer.Append(current);
                current = next();
            }
        }

        private void tokenizeOneLineComment()
        {
            char current = peek(0);
            while ("\r\n\0".IndexOf(current) == -1) current = next();
    }

        private void tokenizeMultilineComment()
        {
            char current = peek(0);
            while (true) {
                if (current == '\0') throw new Exception("Closing tag is missing");
                if (current == '*' && peek(1) == '/') break;
                current = next();
            }
            next();
            next();
        }

        private void tokenizeNumber()
        {
            char current = peek(0);
            StringBuilder buffer = new StringBuilder();
            while (true)
            {
                if (current == ',')
                {
                    if (buffer.ToString().IndexOf(',') != -1) throw new Exception("Incorrect float number");
                }
                else if (!Char.IsDigit(current)) break;
                buffer.Append(current);
                current = next();
            }

            AddToken(TokenType.NUMBER, buffer.ToString());
        }

        private void AddToken(TokenType _type) {
            tokens.Add(new Token(_type, ""));
        }

        private void AddToken(TokenType _type, string _text){
            tokens.Add(new Token(_type, _text));
        }

        private char peek(int _relativePosition) {
            int position = pos + _relativePosition;
            if (position >= InputLenght) return '\0';
            else return input[position];
        }

        private char next()
        {
            pos++;
            return peek(0);
        }
    }
      
}
