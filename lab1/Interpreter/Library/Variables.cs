﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter.Library
{
    class Variables
    {
        static Dictionary<string, Value> variables = new Dictionary<string, Value>
        {
            {"PI", new NumberValue(Math.PI) },
            {"pi", new NumberValue(Math.PI) },
            {"E", new NumberValue(Math.E) },
            {"e", new NumberValue(Math.E) },
            { "OWN", new NumberValue(2.28) }
        };
        public static bool exists(string key)
        {
            return variables.ContainsKey(key);
        }

        public static Value get(string key)
        {
            if (!exists(key)) return new NumberValue(0);
            else return variables[key];
        }

        public static void set(string key, Value value)
        {
            if (!exists(key))
                variables.Add(key, value);
            else variables[key] = value;
        }
    }

}
