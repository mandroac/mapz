﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Interpreter.Library
{
    class Functions
    {
        static Dictionary<string, Function> functions = new Dictionary<string, Function>
        {
            {"sin" , new SinFunction() },
            {"cos" , new CosFunction() },
            {"openUrl" , new OpenUrl() },
            {"GoogleSearch" , new GoogleSearch() },
            {"sleep" , new SleepFunction() },


        };
        public static bool exists(string key)
        {
            return functions.ContainsKey(key);
        }

        public static Function  get(string key)
        {
            if (!exists(key)) throw new Exception("Function " + key +  " does not exist");
            else return functions[key];
        }

        public static void set(string key, Function function)
        {
            if (!exists(key))
                functions.Add(key, function);
            else throw new Exception("Function " + key + " already exists");
        }

    }

    internal class SleepFunction : Function
    {
        public SleepFunction()
        {
        }

        public Value execute(params Value[] args)
        {
            if (args.Length != 1) throw new Exception("1 argument is needed");
            else {
                Thread.Sleep((int)args[0].asDouble() * 1000);
                return new NumberValue(0);
            }
        }
    }

    internal class GoogleSearch : Function
    {
        public GoogleSearch()
        {
        }

        public Value execute(params Value[] args)
        {
            if (args.Length != 1) throw new Exception("1 argument is needed");
            else return new StringValue("https://www.google.com/search?q=" + args[0].asString()); 
        }
    }

    class SinFunction : Function
    {
        public SinFunction()
        {
        }

        public Value execute(params Value[] args)
        {
            if (args.Length != 1) throw new Exception("1 argument is needed");
            else return new NumberValue(Math.Sin(args[0].asDouble()));
        }
    }
    class CosFunction : Function
    {
        public CosFunction()
        {
        }

        public Value execute(params Value[] args)
        {
            if (args.Length != 1) throw new Exception("1 argument is needed");
            else return new NumberValue(Math.Cos(args[0].asDouble()));
        }
    }
    class OpenUrl : Function
    {
        public OpenUrl()
        {
        }
        public static bool ValidHttpURL(string s, out Uri resultURI)
        {
            if (!Regex.IsMatch(s, @"^https?:\/\/", RegexOptions.IgnoreCase))
                s = "http://" + s;

            if (Uri.TryCreate(s, UriKind.Absolute, out resultURI))
                return (resultURI.Scheme == Uri.UriSchemeHttp ||
                        resultURI.Scheme == Uri.UriSchemeHttps);

            return false;
        }

        public Value execute(params Value[] args)
        {
            if (args.Length != 1) throw new Exception("1 argument is needed");
            else {
                Uri uriResult;
                if (ValidHttpURL(args[0].asString(), out uriResult)) return new StringValue(uriResult.AbsoluteUri);
                else throw new Exception("Invalid URL");
            }
        }
    }
}
