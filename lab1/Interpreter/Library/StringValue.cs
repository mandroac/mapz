﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter.Library
{
    class StringValue : Value
    {
        private string value;

        public StringValue(string value)
        {
            this.value = value;
        }

        public double asDouble() 
        {
            double temp;
            if (Double.TryParse(value, out temp)) return temp;
            else throw new Exception("Cannot convert " + value + " to double");
        }

        public string asString()
        {
            return value;
        }

        public override string ToString()
        {
            return asString();
        }
    }
}
