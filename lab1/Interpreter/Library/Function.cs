﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter.Library
{
    public interface Function
    {
        Value execute(params Value[] args);
    }
}
