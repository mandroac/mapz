﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter.Library
{
    class NumberValue : Value
    {
        private double value;

        public NumberValue(double _value)
        {
            value = _value;
        }
        public NumberValue(bool _value)
        {
            value = _value ? 1 : 0;
        }

        public double asDouble()
        {
            return value;
        }

        public string asString()
        {
            return Convert.ToString(value);
        }

        public override string ToString()
        {
            return asString();
        }
    }
}
