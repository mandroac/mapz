﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interpreter.Library;

namespace Interpreter.Statements
{
    public class AssignmentStatement : Statement
    {
        private string variable;
        private Expression expression;

        public AssignmentStatement(string variable, Expression expression)
        {
            this.variable = variable;
            this.expression = expression;
        }

        public Statement execute()
        {
            Value result = expression.Eval();
            Variables.set(variable, result);
            return this;
        }

        public override string ToString()
        {
            return String.Format("{0} = {1}", variable, expression.Eval());
        }
    }
}
