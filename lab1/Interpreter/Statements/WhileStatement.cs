﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter.Statements
{
    class WhileStatement : Statement
    {
        private Expression condition;
        private Statement statement;

        public WhileStatement(Expression condition, Statement statement)
        {
            this.condition = condition;
            this.statement = statement;
        }

        public Statement execute()
        {
            while (condition.Eval().asDouble() != 0) statement.execute();
            return (Statement)this;
        }

        public override string ToString()
        {
            return "while(" + condition + ") " + statement;
        }
    }
}
