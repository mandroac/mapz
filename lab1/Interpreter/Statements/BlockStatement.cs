﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter.Statements
{
    class BlockStatement : Statement
    {
        private List<Statement> statements;

        public BlockStatement()
        {
            this.statements = new List<Statement>();
        }

        public void add(Statement statement) {
            statements.Add(statement);
        }

        public Statement execute()
        {
            foreach (Statement statement in statements) statement.execute();
            return (Statement)this;
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            foreach (Statement statement in statements) result.Append(statement.ToString()).Append("\n");

            return result.ToString();
        }
    }
}
