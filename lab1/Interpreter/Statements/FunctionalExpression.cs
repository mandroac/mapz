﻿using Interpreter.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter.Statements
{
    class FunctionalExpression : Expression
    {
        private string name;
        private List<Expression> arguments;

        public FunctionalExpression(string name)
        {
            this.name = name;
            this.arguments = new List<Expression>();

        }

        public FunctionalExpression(string name, List<Expression> arguments)
        {
            this.name = name;
            this.arguments = arguments;
        }

        public void addArgument(Expression arg)
        {
            arguments.Add(arg);
        }

        public Value Eval()
        {
            int size = arguments.Count;
            Value[] values = new Value[size];
            for (int i = 0; i < size; ++i) {
                values[i] = arguments[i].Eval();
            }
            return Functions.get(name).execute(values);

        }

        public override string ToString()
        {
            return name + "(" + arguments.ToString() + ")";
        }
    }
}
