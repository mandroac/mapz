﻿using Interpreter.Statements;

namespace Interpreter
{
    class FunctionalStatement : Statement
    {
        private FunctionalExpression function;

        public FunctionalStatement(FunctionalExpression function)
        {
            this.function = function;
        }

        public Statement execute()
        {
            function.Eval();
            return this;

        }

        public override string ToString()
        {
            return function.ToString();
        }
    }
}