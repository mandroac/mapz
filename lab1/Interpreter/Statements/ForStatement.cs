﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter.Statements
{
    class ForStatement : Statement
    {
        private Statement initialization;
        private Expression termination;
        private Statement increment;
        private Statement block;

        public ForStatement(Statement initialization, Expression termination, Statement increment, Statement block)
        {
            this.initialization = initialization;
            this.termination = termination;
            this.increment = increment;
            this.block = block;
        }

        public Statement execute()
        {
            for (initialization.execute(); termination.Eval().asDouble() != 0; increment.execute())
                block.execute();
            return (Statement)this;
        }

        public override string ToString()
        {
            return "for(" + initialization + "; " + termination + " ;" + increment + " \n" + block;
        }
    }
}

