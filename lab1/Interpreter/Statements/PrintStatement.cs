﻿using Interpreter.Statements;
using System;

namespace Interpreter
{
    public class PrintStatement : Statement
    {
        private Expression expression;

        public PrintStatement(Expression expression)
        {
            this.expression = expression;
        }

        public Statement execute()
        {
            Console.WriteLine(expression.Eval());
            return this;
        }

        public override string ToString()
        {
            return "print " + expression;
        }
    }
}