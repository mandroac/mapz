﻿using Interpreter.Expressions;
using Interpreter.Statements;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter
{

    class FunctionArgs { 
        public FunctionalExpression FunctionalExpression { get; set; }
    }
    class Parser
    {
        static Token EOF = new Token(TokenType.EOF , " ");
        
        private List<Token> tokens;
        private int pos, size;

        public Parser(List<Token> _tokens) {
            tokens = _tokens;
            size = _tokens.Count;
        }

        public event EventHandler<FunctionArgs> FunctionIsCalled;
        protected virtual void onFunctionIsCalled(FunctionalExpression expression)
        {
            FunctionIsCalled?.Invoke(this, new FunctionArgs() { FunctionalExpression = expression });
        }

        public List<Statement> parse() {
            List<Statement> result = new List<Statement>();
            while (!match(TokenType.EOF))
                result.Add(statement());
            return result;
        }

        private Statement block() {
            BlockStatement block = new BlockStatement();
            consume(TokenType.LBRACE);
            while (!match(TokenType.RBRACE)) block.add(statement());
            return block;
        }

        private Statement statementOrBlock() {
            if (get(0).type == TokenType.LBRACE) return block();
            else return statement();
        }
        private Statement statement()
        {
            if (match(TokenType.PRINT)) return new PrintStatement(expression());
            if (match(TokenType.IF)) return IfElseStatement();
            if (match(TokenType.WHILE)) return WhileStatement();
            if (match(TokenType.FOR)) return ForStatement();
            if (get(0).type == TokenType.WORD && get(1).type == TokenType.LPAREN) 
            { 
                var functionalExpression = function();
                onFunctionIsCalled(functionalExpression);
                return new FunctionalStatement(functionalExpression);
               
            }

            return assignmentStatement();
        }
        private Statement assignmentStatement()
        {
            Token current = get(0);
            if ( match(TokenType.WORD) && get(0).type == TokenType.EQ)
            {
                string variable = current.text;
                consume(TokenType.EQ);
                return new AssignmentStatement(variable, expression());
            }
            else throw new Exception("Unknown statement");
        }

        private Statement IfElseStatement()
        {
            Expression condition = expression();
            Statement ifStatement = statementOrBlock();
            Statement elseStatement;
            if (match(TokenType.ELSE)) elseStatement = statementOrBlock();
            else elseStatement = null;
            return new IfStatement(condition, ifStatement, elseStatement);
        }
        private Statement ForStatement()
        {
            Statement initialization = assignmentStatement();
            consume(TokenType.SEMICOL);
            Expression termination = expression();
            consume(TokenType.SEMICOL);
            Statement increment = assignmentStatement();
            Statement statement = statementOrBlock();

            return new ForStatement(initialization, termination, increment, statement);
        }
        private Statement WhileStatement()
        {
            Expression condition = expression();
            Statement statement = statementOrBlock();
            return new WhileStatement(condition, statement);
        }

        private FunctionalExpression function()
        {
            string name = consume(TokenType.WORD).text;
            consume(TokenType.LPAREN);
            FunctionalExpression function = new FunctionalExpression(name);
            while (!match(TokenType.RPAREN))
            {
                function.addArgument(expression());
                match(TokenType.COMMA);
            }
            return function;
        }

        private Expression expression()
        { 
            return equality();
        }
        private Expression equality()
        {
            Expression result = conditional();

            if (match(TokenType.EQEQ))
            {
                return new ConditionalExpression(ConditionalExpression.Operator.EQUALS, result, conditional());
            }
            if (match(TokenType.EXCLEQ))
            {
                return new ConditionalExpression(ConditionalExpression.Operator.NOT_EQUALS, result, conditional());
            }
           
            return result;
        }

            private Expression conditional() {
            Expression result = additive();

            while (true)
            {
               
                if (match(TokenType.LT))
                {
                    result = new ConditionalExpression(ConditionalExpression.Operator.LT, result, additive());
                    continue;
                }
                if (match(TokenType.LTEQ))
                {
                    result = new ConditionalExpression(ConditionalExpression.Operator.LTEQ, result, additive());
                    continue;
                }
                if (match(TokenType.GT))
                {
                    result = new ConditionalExpression(ConditionalExpression.Operator.GT, result, additive());
                    continue;
                }
                if (match(TokenType.GTEQ))
                {
                    result = new ConditionalExpression(ConditionalExpression.Operator.GTEQ, result, additive());
                    continue;
                }
                break;
            }
            return result;
        }

        private Expression additive()
        {
            Expression result = multiplicative();

            while (true)
            {
                if (match(TokenType.PLUS))
                {
                    result = new BinaryExpression('+', result, multiplicative());
                    continue;
                }
                if (match(TokenType.MINUS))
                {
                    result = new BinaryExpression('-', result, multiplicative());
                    continue;
                }
                break;
            }
            return result;
        }

        private Expression multiplicative()
        {
            Expression result = unary();

            while (true) {
                if (match(TokenType.MULT)) {
                    result =  new BinaryExpression('*', result, unary());
                    continue;
                }
                if (match(TokenType.DIV))
                {
                    result = new BinaryExpression('/', result, unary());
                    continue;
                }
                break;
            }
            return result;
        }
        private Expression unary()
        {
            if (match(TokenType.MINUS)) {
                return new UnaryExpression('-',primary());
            }
            if (match(TokenType.PLUS))
            {
                return new UnaryExpression('+', primary());
            }
            return primary();
        }
        private Expression primary() {
            Token current = get(0);
            if (match(TokenType.NUMBER)) return new ValueExpression(Double.Parse(current.text));
            if (get(0).type == TokenType.WORD && get(1).type == TokenType.LPAREN) return function();
            if (match(TokenType.WORD)) return new VariableExpression(current.text);
            if (match(TokenType.TEXT)) return new ValueExpression(current.text);
            if (match(TokenType.LPAREN)) {
                Expression result =  expression();
                match(TokenType.RPAREN);
                return result;
            } 

            else throw new Exception("Unknown expression");
        }

        private bool match(TokenType type) {
            Token current = get(0);
            if (type == current.type)
            {
                pos++; return true;
            }
            else return false;  
        }

        private  Token get(int _relativePosition)
        {
            int position = pos + _relativePosition;
            if (position >= size) return EOF;
            else return tokens[position];
        }

        private Token consume(TokenType type)
        {
            Token current = get(0);
            if (type == current.type)
            {
                pos++; return current;
            }
            else throw new Exception("Token " + current.type + " doesn't match type " + type);
        }
    }
}
