﻿using System;
/*using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;*/

namespace Interpreter
{
    enum TokenType { 
        NUMBER,
        WORD,
        TEXT,

        PLUS,
        MINUS, 
        MULT,
        DIV,
        EQ,
        EQEQ,
        EXCLEQ,
        LT,
        LTEQ,
        GT,
        GTEQ,

        //keywords
        PRINT,
        IF, 
        ELSE,
        FOR,
        WHILE,

        LPAREN,  // (
        RPAREN,  // )
        LBRACE,  // {
        RBRACE,  // }
        SEMICOL, // ;
        COMMA,   // ,


        
        EOF
    };
}

